## Week 1: 10/26 - 10/30

- collaborated on [blog post about managing Spam](https://about.gitlab.com/blog/2020/10/29/how-we-work-to-detect-and-mitigate-spam/) in response to WikiMedia hackernews thread  https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/general/-/issues/67#note_438656119
- collaborated on blog posts and finding solutions to docker hub rate limiting see #dockerhub-pull-rate-self-managed [#support_managers](https://gitlab.slack.com/archives/CBVAE1L48/p1603985875283200)
- MR to fix issue with [anchor link for renewals](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/66497) on OSS program page 
- MR to fix issue important to WikiMedia migration/adoption of GitLab https://gitlab.com/gitlab-org/gitlab/-/merge_requests/46070
- MVC of ticket deflection metrics in Support Group Conversation Slides and Conversation

## Week 2: 11/1 - 11/7

- presented MVC of ticket deflection in Support Group Conversation
- met with David Planella on:
  - open source support liason role
  - OSS program projects and opportunities
  - biweekly marketing updates
  - AMA with advocates *and* support
- met with Technical writing team re: methods to measure Ticket Deflection through Documentation
  - ideas:
    - consider customers linking Support to docs in tickets as a signal for deficiency or unclear documentation
    - gather data on what links support most commonly shares with customers
    - cross-examine links support shares with customers and the Google search terms folks use to find documentation
    - examine how many people go from docs landing page to exit page on Support Portal
- meeting with OSS program manager on:
  - how to increase efficiency of vailidating renewals and applications for partners and program members
  - automating license scan
  - requirements for OSS Partners
  - ideas for changes to OSS programs
  - ideas for 
- [Community Ops meeting](https://docs.google.com/document/d/1gs98xTgjEMEye_5eT6uGpemC9Ck4MxhIHpaIzuTz6Wk/edit#) on Thursday on:
  - measuring documentation as method of ticket deflection
  - forum as primary method of ticket deflection in the wild
  - Forum Admin roles and settings
  - using Community Forum as a place to direct feedback and questions on changes